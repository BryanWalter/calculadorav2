/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Calculadora;

/**
 *
 * @author Walter PC
 */
@WebServlet(name = "CalculadoraController", urlPatterns = {"/CalculadoraController"})
public class CalculadoraController extends HttpServlet {

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    response.setContentType("text/html;charset=UTF-8");
    try (PrintWriter out = response.getWriter()) {
      /* TODO output your page here. You may use following sample code. */
      out.println("<!DOCTYPE html>");
      out.println("<html>");
      out.println("<head>");
      out.println("<title>Servlet CalculadoraController</title>");      
      out.println("</head>");
      out.println("<body>");
      out.println("<h1>Servlet CalculadoraController at " + request.getContextPath() + "</h1>");
      out.println("</body>");
      out.println("</html>");
    }
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    
        System.out.println("Capital " + request.getParameter("Capital"));
        System.out.println("Interes anual " + request.getParameter("Tasa_anual"));
        System.out.println("Años " + request.getParameter("Annos"));
        
        int Capital= Integer.parseInt(request.getParameter("Capital"));
        int Tasa_anual= Integer.parseInt(request.getParameter("Tasa_anual"));
        int Annos= Integer.parseInt(request.getParameter("Annos"));
        
        Calculadora cal=new Calculadora();
        cal.getInteres_simple();
        cal.resultado(Integer.parseInt(request.getParameter("Capital")),Integer.parseInt(request.getParameter("Tasa_anual")),Integer.parseInt(request.getParameter("Annos")));
        request.setAttribute("Calculadora", cal);
        request.getRequestDispatcher("Resultado.jsp").forward(request, response);
        
        System.out.println("Resultado " + cal.getInteres_simple());
        
        
        
        
          
          
  
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "Short description";
  }// </editor-fold>

}
