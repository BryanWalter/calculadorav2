/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author Walter PC
 */
public class Calculadora {
  
    private int Interes_simple;
    private int Capital;
    private int Annos;
    private int Tasa_anual;

  /**
   * @return the Interes_simple
   */
  public int getInteres_simple() {
    return Interes_simple;
  }

  /**
   * @param Interes_simple the Interes_simple to set
   */
  public void setInteres_simple(int Interes_simple) {
    this.Interes_simple = Interes_simple;
  }

  /**
   * @return the Capital
   */
  public int getCapital() {
    return Capital;
  }

  /**
   * @param Capital the Capital to set
   */
  public void setCapital(int Capital) {
    this.Capital = Capital;
  }

  /**
   * @return the Annos
   */
  public int getAnnos() {
    return Annos;
  }

  /**
   * @param Annos the Annos to set
   */
  public void setAnnos(int Annos) {
    this.Annos = Annos;
  }

  /**
   * @return the Tasa_anual
   */
  public int getTasa_anual() {
    return Tasa_anual;
  }

  /**
   * @param Tasa_anual the Tasa_anual to set
   */
  public void setTasa_anual(int Tasa_anual) {
    this.Tasa_anual = Tasa_anual;
  }


    public int calcular (){
      return this.Interes_simple= this.getCapital()*(this.getTasa_anual()/100)*this.getAnnos(); 
      
    }
    
    public int resultado(int Capital, int Tasa_anual, int Annos ){
      this.setCapital(Capital);
      this.setTasa_anual(Tasa_anual);
      this.setAnnos(Annos);
      
      return calcular();
    }
  
}
